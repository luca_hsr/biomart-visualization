
var chart 
var B = BiomartVisualization
var newChartEnv = {
    setup: function() {
        var svg = d3.select('#qunit-fixture').append('svg')
        chart = BiomartVisualization.Chart(svg)
    },
    teardown: function() {
        chart = null
    }
}


///////
module ('Char constructor')

test ('when no argument is provided', function() {
    expect(1)

    throws (function() { B.Chart() }, "throws an error")
})

test ('if argument is an (d3) svg element', function() {
    expect(1)

    var chart = B.Chart(d3.select('#qunit-fixture').append('svg'))
    ok (!d3.select('#qunit-fixture').select('svg').select('g').empty(), 'appends a group element to the <svg>')
})

test ('if argument is a (d3) group element', function() {
    expect(2)

    var g = d3.select('#qunit-fixture').append('svg').append('g')
        .attr('id', 'chart')
    var chart = B.Chart(g)

    chart = d3.select('#qunit-fixture').select('svg').selectAll('g')
    equal (chart.size(), 1, 'does nothing')
    equal (chart.attr('id'), 'chart')
})

test ('if argument is an svg element', function() {
    expect(1)

    var chart = d3.select('#qunit-fixture').append('svg:svg').node()
    chart.id = 'chart'

    B.Chart(chart)
    equal (d3.select('#chart').selectAll('g').size(), 1, 'appends a group element to the <svg>')
})

test ('if argument is a group element', function() {
    var frag = document.createDocumentFragment()
    var chart = document.createElementNS('http://www.w3.org/2000/svg', 'svg')
    chart.id = 'chart'
    frag.appendChild(chart)
    var g = document.createElementNS('http://www.w3.org/2000/svg', 'g')
    g.id = 'group'
    chart.appendChild(g)
    document.getElementById('qunit-fixture').appendChild(frag)
    ok (document.getElementById('group'))
    var chart = B.Chart(d3.select('#group').node())

    equal (d3.select('#chart').selectAll('g').size(), 1, 'does nothing')
})


///////
module ('A new Chart', newChartEnv)

test ('has a property canvas of type d3.selection', function() { 
    ok (chart.canvas) 
    ok (chart.canvas instanceof d3.selection)
})

test ('has no drawn elements', function() { ok (!chart.els) })

test ('can be assigned elements', function() {
    addElms(chart, 'rect', 3)
    ok (chart.els)
    equal (chart.els.size(), 3)
})


///////
module ('Chart Settings', newChartEnv)

test ('are initially empty by defualt', function() {
    expect(2)

    ok (chart.settings(), "settings object exists")
    deepEqual (chart.settings(), {}, "and is an empty object")
})

test ('when submitted are merged with default settings', function() {
    expect(2)

    chart.settings({ radius: 40 })
    chart.settings({ n: 42 })
    ok (chart.settings(), "settings object exists")
    deepEqual (chart.settings(), { radius: 40, n: 42 }, "and has merged values")
})


///////
var evt

module ('Event Handlers', {
    setup: function() {
        newChartEnv.setup()
            evt = document.createEvent('MouseEvent')
            evt.initMouseEvent('click', true, true, window, 1, 0, 0)
    },
    teardown: function() {
        newChartEnv.teardown()
        evt = null 
    }
})

asyncTest ('calling .addListeners(), listeners bound prior of creating elements are applied when created', 
    function() {
    expect(1)   

    chart.on('click', function() { ok(1); start() })
    addElms(chart, 'rect', 1)

    chart.addListeners()

    // ok (chart.canvas.select('rect').on('click'), 'the listeners are bound')
    chart.els.each(function() { this.dispatchEvent(evt) })
})

asyncTest ('when multiple listeners are bound all get called in order', 
    function() {
    expect(2)

    chart.on('click', function() { ok(1); })
    chart.on('click', function() { ok(1); start() })
    addElms(chart, 'rect', 1)

    chart.addListeners()

    chart.els.each(function() { this.dispatchEvent(evt) })
})

asyncTest ('if elements are already presents: listeners are immediately bound', 
    function() {
    expect(1)   

    addElms(chart, 'rect', 1)
    chart.on('click', function() { ok(1); start() })

    // ok (chart.canvas.select('rect').on('click'), 'the listeners are bound')
    chart.els.each(function() { this.dispatchEvent(evt) })
})

asyncTest ('if elements are already presents: when multiple listeners are bound each one get called in order', 
    function() {
    expect(2)

    addElms(chart, 'rect', 1)
    chart.on('click', function() { ok(1); })
    chart.on('click', function() { ok(1); start() })

    chart.els.each(function() { this.dispatchEvent(evt) })
})

