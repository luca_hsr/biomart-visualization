
var bars 
var svg 
var noop
var newBarsEnv = {
    setup: function() {
        svg = d3.select('#list-of-diseases').append('svg')
        bars = BiomartVisualization.HBarChart(svg)
    },
    teardown: function() {
        bars = svg = null
        noop = function() {}
    }
}


///////
module ('Bars Chart Methods', newBarsEnv)

test ('.max() initially returns undefined', function() {
    ok (!bars.max())
})

test ('.barLength() return the assigned value if function or a function returning value', function() {
    bars.barLength(noop)
    strictEqual (bars.barLength(), noop)
    bars.barLength(42)
    strictEqual (bars.barLength()(), 42)
})

test ('.data() return the assigned value', function() {
    a = [1,2,3,4]
    bars.data(a)
    strictEqual (bars.data(), a)
})

test ('.color() return the assigned value', function() {
    bars.color(noop)
    strictEqual (bars.color(), noop)
})

test ('.text() return the assigned value', function() {
    bars.text(noop)
    strictEqual (bars.text(), noop)
})

test ('.id() return the assigned value', function() {
    bars.id(noop)
    strictEqual (bars.id(), noop)
})

test ('given the data and barLength, .max() returns the actual maximum value', function () {
    var d = [-2, 0, 1]
    var f = function(d) { return d }
    var m = d3.max(d, f)
    bars.data(d)
    bars.barLength(f)
    equal (bars.max(), m)
})

test ('.y() return the assigned value', function() {
    bars.y(noop)
    strictEqual (bars.y(), noop)
})

test ('.canvas() returns the group element inside which we draw', function() {
    strictEqual (bars.canvas().node(), svg.select('g').node())
})


///////
var c = d3.scale.category20()
var color = function(d) { return c(d.color) }
var id = function(d) { return d.id }
var text 
var length = function(d) { return d.length }
var y = function(d) { return d.color * 3 }
module ('Bars Chart .draw()', {
    setup: function() {
        newBarsEnv.setup()
        bars.color(color)
        bars.id(id)
        
        if (text) {
            bars.settings({ text: true })
            bars.text(text)
        } 
        
        bars.y(y)
        bars.barLength(length)

        bars.data([
            { id: 'a', color: 1, length: 4 },
            { id: 'b', color: 2, length: 5 },
            { id: 'c', color: 3, length: 6 }
        ])

        bars.draw()
    },
    teardown: function() { newBarsEnv.teardown() }
})

test ('draws the proper number of bars', function() {
    equal (svg.selectAll('rect').size(), 3, 'the number of bars is right')
})

asyncTest ('draws the bars with the right properties', function() {
    var th = bars.settings().thickness

    setTimeout (function() {
        svg.selectAll('rect').each(function(d) {
            equal (this.getAttribute('x'), null, 'returns null because we set x on the group (which is actually positioning the group')
            equal (+this.getAttribute('y'), y(d), 'right position on the list')
            equal (+this.getAttribute('width'), length(d), 'as long as specified')
            equal (+this.getAttribute('height'), th, 'thick as specified')
            equal (this.getAttribute('fill'), color(d), 'with the color we chose')
        })
    }, 10)
    start()
})

test ('without text', function() {
    strictEqual (svg.selectAll('text').size(), 0, 'no <text> found')
})

test = function(d) { return d.id }
test ('or with text if explicitly specified', function() {
    strictEqual (svg.selectAll('text').size(), 3, '<text> found')    
})




