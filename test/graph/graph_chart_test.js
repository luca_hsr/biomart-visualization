
var graph 
var svg 
var newGraphEnv = {
    setup: function() {
        svg = d3.select('#graph-tab').append('svg')
        graph = BiomartVisualization.GraphChart(svg)
    },
    teardown: function() {
        graph = svg = null
    }
}

module ('Graph Chart Methods', newGraphEnv)

test ('.nodes() returns the assigned nodes', function() {
    var nodes = [1,2,3]
    graph.nodes(nodes)
    deepEqual (graph.nodes(), nodes)
})

test ('.linkes() returns the assigned links', function() {
    var links = [1,2,3]
    graph.links(links)
    deepEqual (graph.links(), links)
})

test ('.id() returns the assigned id', function() {
    var id = function() {}
    graph.id(id)
    deepEqual (graph.id(), id)
})

test ('.color() returns the assigned color', function() {
    var color = function() {}
    graph.color(color)
    deepEqual (graph.color(), color)
})

test ('.text() returns the assigned test', function() {
    var text = function() {}
    graph.text(text)
    deepEqual (graph.text(), text)
})

test ('.canvas() returns the main group element', function() {
    equal (graph.canvas().node(), svg.node().childNodes[0])
})


///////
var c = d3.scale.category20()
var color = function(d) { return c(d.color) }
var id = function(d) { return d.id }
var text = function(d) { return d.id }
module ('Graph Chart .draw()', {
    setup: function() {
        newGraphEnv.setup()
        graph.color(color)
        graph.id(id)
        graph.text(text)

        graph.nodes([
            { id: 'a', color: 1 },
            { id: 'b', color: 2 },
            { id: 'c', color: 3 }
        ])

        graph.links([
            { source: 0, target: 1 },
            { source: 1, target: 2 },
            { source: 2, target: 0 }
        ])

        graph.draw()
    },
    teardown: function() { newGraphEnv.teardown() }
})

function drawGraphTest (argument) {
    test ('draws the proper number of bubbles and links', function() {
        equal (svg.selectAll('circle').size(), 3)
        equal (svg.selectAll('line').size(), 3)
    })

    asyncTest ('draws bubbles with proper properties', function() {

        var s = graph.settings()
        var c = 0

        svg.selectAll('circle').each(function(d) {
            equal (this.getAttribute('r'), s.radius)
            equal (this.getAttribute('class'), s.nodeClassName)
            equal (this.getAttribute('fill'), color(d))
            if (++c === 3) start()
        })
    })

    asyncTest ('draws the right links', function() {
        // expect(svg.selectAll('line').size() * 6)

        setTimeout(function() {
            svg.selectAll('line').each(function(d) {
                var nodes = graph.nodes()
                deepEqual (d.source, nodes[d.source.index])
                deepEqual (d.target, nodes[d.target.index])

                equal (d.source.x, +this.getAttribute('x1'))
                equal (d.source.y, +this.getAttribute('y1'))
                equal (d.target.x, +this.getAttribute('x2'))
                equal (d.target.y, +this.getAttribute('y2'))
            })

            start()

        }, 100)
    })
}

drawGraphTest()

test ('there is no text', function() {
    equal (d3.selectAll('text').size(), 0, 'no <text>s found')
})

///////
module ('Graph Chart .draw() with text',{
    setup: function() {
        newGraphEnv.setup()
        graph.settings({text: true})
        graph.color(color)
        graph.id(id)
        graph.text(text)

        graph.nodes([
            { id: 'a', color: 1 },
            { id: 'b', color: 2 },
            { id: 'c', color: 3 }
        ])

        graph.links([
            { source: 0, target: 1 },
            { source: 1, target: 2 },
            { source: 2, target: 0 }
        ])

        graph.draw()
    },
    teardown: function() { newGraphEnv.teardown() }
})

drawGraphTest()

asyncTest ('draws the text', function() {

    setTimeout(function() {
        svg.selectAll('g').selectAll('text').each(function(d) {
            equal (text(d), this.textContent)
        })

        start()

    }, 10)
})



