function addElms (chart, tag, number, data) {
    var d 
    if (!(d = data)) {
        d = []
        for (var i = 1; i <= number; ++i)
            d.push(i)
    }
    return chart.els = chart.canvas.selectAll(tag)
        .data(d)
        .enter()
        .append(tag)
}