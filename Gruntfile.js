module.exports = function(grunt) {
  'use strict';

  // Project configuration.
  grunt.initConfig({
    connect: {
      test: {
        options: {
          port: 9001,
          base: '.',
          keepalive: true
        }
      }
    },

    qunit: {
      all: ['test/**/*.html'],
      chart: ['test/chart/**/*.html'],
      graph: ['test/graph/**/*.html'],
      bars: ['test/bars/**/*.html']
    },

    watch: {
      all: {
        files: ['test/**/*', 'js/lib/**/*'],
        tasks: ['qunit:all']
      },
      chart: {
        files: ['test/chart/**/*', 'js/lib/**/*'],
        tasks: ['qunit:chart']
      },
      graph: {
        files: ['test/graph/**/*', 'js/lib/**/*'],
        tasks: ['qunit:graph']
      },
      bars: {
        files: ['test/bars/**/*', 'js/lib/**/*'],
        tasks: ['qunit:bars']
      }
    }

    // ,
    // jshint: {
    //   all: [
    //     'Gruntfile.js',
    //     'js/**/*.js',
    //     'spec/**/*.js'
    //   ],
    //   options: {
    //     jshintrc: '.jshintrc'
    //   }
    // }
  });

  grunt.loadNpmTasks('grunt-contrib-qunit');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  // grunt.loadNpmTasks('grunt-contrib-jshint');

  grunt.registerTask('test', ['qunit']);
  grunt.registerTask('server', ['connect']);
  // grunt.registerTask('watch', ['qunit:all']);
  // grunt.registerTask('watch:chart', ['qunit:chart']);
  // grunt.registerTask('watch:graph', ['qunit:graph']);

  grunt.registerTask('default', ['watch']);

};