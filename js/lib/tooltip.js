BiomartVisualization.Tooltip = (function (d3) {
"use strict"

var tooltip_ttCounter = 0

function D3Tooltip () {
  this.id = 'd3-tooltip-' + tooltip_ttCounter
  this.class = 'd3-tooltip'
  this.$el =  d3.select('body').append('div')
                .attr('class', this.class)
                .attr('id', this.id)
                .style('opacity', 0)
                .style('position', 'absolute')
                .style('pointer-events', 'none')

  tooltip_ttCounter += 1
}

D3Tooltip.prototype.html = function(html) {
  this.$el.html(html)
}

D3Tooltip.prototype.show = function() {
  this.$el.transition().duration(200).style('opacity', .9)
  this.$el.style('left', (d3.event.pageX + 30) + 'px')
          .style('top', (d3.event.pageY - 28) + 'px')

}

D3Tooltip.prototype.hide = function() {
  this.$el.transition().duration(500).style('opacity', 0)
}

return D3Tooltip

})(d3)
