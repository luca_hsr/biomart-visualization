var BiomartVisualization = {}

!function(bs) {
    var metrics = bs.Metrics = {}

    metrics.mainPanelWidthRate = 0.7
    metrics.mainPanelHeightRate = 0.8
    metrics.initialViewportW = verge.viewportW()
    metrics.initialViewportH = verge.viewportH()
    metrics.mainPanelWidth = metrics.initialViewportW * metrics.mainPanelWidthRate
    metrics.mainPanelHeight = metrics.initialViewportH * metrics.mainPanelHeightRate
    
}(BiomartVisualization)