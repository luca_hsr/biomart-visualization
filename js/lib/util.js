BiomartVisualization.Util = (function (d3) {
"use strict"

return {
    translate: function(x, y) { return ' translate('+ x +','+ (y ? y : 0) +')' },
    
    scale: function(x, y) { return ' scale('+ x +','+ (y ? y : x) +')' },
    
    inherit: function(ctor, superCtor) {
        ctor.prototype = Object.create(superCtor.prototype, {
            constructor: {
                value: ctor,
                configurable: true,
                writable: true,
                enumerable: false
            }
        })
        ctor.super_ = superCtor
    },
    
    // Both target and source are d3 selection.
    // It concatenates the selection of target and source
    concatSelection: function(target, source) {
        // if (! target) return source
        return push.apply(target[0], source[0])
    },
    
    // It removes `elem` from the document and `selection`
    remove: function(selection, elem) {
        var node = elem instanceof d3.selection ? elem.node() : elem,
            sel = selection[0]
        
        var idx = this.indexOf(selection, node)
        if (idx > -1) {
            sel.splice(idx, 1)
            elem.parentNode.removeChild(elem)
            return elem
        }

        return null
    },
    
    indexOf: function(selection, elem) {
        var node = elem instanceof d3.selection ? elem.node() : elem,
            sel = selection[0]
        
        for (var i = 0, l = selection.size(); i < l; ++i) {
            if (sel[i] === node)
                return i
        }

        return -1
    },

    clone: function(_) {
        return JSON.parse(JSON.stringify(_))
    },

    normalizeId: function (id) {
        return id.replace(/[^\da-zA-Z-]/g, '')
    }

}


})(d3)
