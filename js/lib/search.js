
var SearchBar = (function () {
"use strict"

function searchbar (workerScriptPath) {

    // var workers = []
    var w
    var cb

    function search (pattern, elements, callback) {
        cb = callback
        w && w.terminate()
        w = new Worker(workerScriptPath)
        w.onmessage = getResults
        w.postMessage({ pattern: pattern, elements: JSON.stringify(elements) })
    }

    function getResults (e) {
        cb.call(null, JSON.parse(e.data))
    }

    return {
        search: search
    }

}

return searchbar
})()