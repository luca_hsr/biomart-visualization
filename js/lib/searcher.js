

// searcher.js

!function() {
"use strict"

var data
var pattern
var matches = []

function process () {
    var d

    for (var i = 0, len = data.length; i < len; ++i) {
        // For now is a substring search. Would be nice to implement an 
        // inexact pattern matching... (shift-and?)
        if ((d = data[i]).key.indexOf(pattern) > -1) {
            matches.push(d)
        }
    }

    postMessage(JSON.stringify(matches))
}


onmessage = function mexListener (e) {
    // array of d3 elements
    data = JSON.parse(e.data.elements)
    pattern = e.data.pattern
    if (! (data && pattern))
        return

    process()
}


}()