BiomartVisualization.Graph = (function (bs) {
"use strict"

function Graph (svg, data, settings) {
    // var initViewportW = verge.viewportW()
    // var initViewportH = verge.viewportH()
    // var width = +svg.attr('width')
    // var height = +svg.attr('height')

    var graphchart = bs.GraphChart(svg)
    graphchart.settings(settings.chartSettings)

    var viewBox = '0 0 '
        + svg.attr('width') + ' '
        + svg.attr('height')
    svg.attr('viewBox', viewBox)
    svg.attr('preserveAspectRatio', 'xMinYMin meet')

    return graphchart
        .nodes(data.nodes)
        .links(data.edges)
        .id(settings.id)
        .color(settings.color)
        // .resize(mainPanelWidthRate, function(newWidth) {
        //     return newWidth / initViewportW })
        // .scale(width / initViewportW, height / initViewportH)
}

return Graph

})(BiomartVisualization)
