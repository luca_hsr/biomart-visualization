BiomartVisualization.GraphChart = (function(bs) {
"use strict"

function GraphChart (svg) {
    var chart = bs.Chart(svg)
    chart.settings({
        radius: 20,
        linkDistance: 100,
        // linkStrength: 1,
        charge: -500,//-400,
        gravity: 0.06, // default 0.1
        nodeClassName: 'graph-chart-node',
        edgeClassName: 'graph-chart-edge',
        width: 100, height: 100,
        text: false })

    var edges
    var _nodes
    var colorScale
    var idFunc
    var force = d3.layout.force()
    var circles 
    var groups
    var lines
    var text
    var textFunc

    var graph = {
        nodes: nodes,
        links: links,
        draw: draw,
        canvas: canvas,
        id: id,
        color: color,
        text: textFunctor
    }

    d3.rebind(graph, chart, 'on', 'translate', 'scale', 'resize', 'settings')

    // `id` must be a function or a property key.
    // `color` is optional and must be an integer or a function that given
    // a node returns an integer.
    // See `d3.selection.attr` docs.

    function nodes (collection) {
        if (arguments.length < 1)
            return _nodes

        _nodes = collection
        
        return graph
    }

    function links (collection) {
        if (arguments.length < 1) 
            return edges 

        edges = collection
        
        return graph
    }

    function color (value) {
        if (arguments.length < 1) 
            return colorScale

        colorScale = value

        return graph
    }

    function id (value) {
        if (arguments.length < 1) 
            return idFunc
                    
        idFunc = value

        return graph
    }

    function draw() {
        doDraw()
        chart.addListeners()

        return graph
    }

    function doDraw() {
        makeLayout()
        makeLines()
        makeBubbleTextGroups()
        startSym()
    }

    function makeLayout() {
        var cs = chart.settings()

        force = d3.layout.force()
            .nodes(_nodes) 
            .links(edges)
            .size([cs.width, cs.height])
            // .gravity(cs.grabity)
            .linkDistance(cs.linkDistance) // px
            // .linkStrength(cs.linkStrength)
            .charge(cs.charge)
    }

    // A group with a circle and a text for each data.
    function makeBubbleTextGroups() {
        var cs = chart.settings()

        groups = chart.els = chart.canvas.selectAll('g')
            .data(_nodes)
        
        var groupEnter = groups.enter()
            .append('g')

        circles = groupEnter
            .append('circle')
            .attr({
                r: cs.radius,
                'class': cs.nodeClassName,
                fill: colorScale })
            .call(force.drag)

        chart.settings().text && groupEnter
            .append('text')
            .attr({
                'text-anchor': 'middle',
                "font-family": "sans-serif",   
                "font-size": 10,
                x: function(d) { return d.cx },
                y: function(d) { return d.cy } })
            .text(textFunc)

        text = groups.selectAll('text')

        // Exit
        // TODO: add transition.
        groups.exit()
            .remove()
    }

    function makeLines() {
        // Update
        lines = chart.canvas.selectAll('line')
            .data(edges)

        // Enter
        lines.enter()
            .append('line')
            .attr('class', chart.settings().edgeClassName)

        // Exit
        lines.exit()
            .remove()
    }

    function startSym() {
        var cs = chart.settings()
        var m = typeof cs.radius === 'function' 
            ? d3.max(circles, cs.radius)
            : cs.radius
        var w = cs.width
        var h = cs.height
        
        force.start()
            .on("tick", function() {
                circles.attr({
                    cx: function(d) { 
                        return d.x = Math.max(m, Math.min(w - m, d.x)) },
                    cy: function(d) { 
                        return d.y = Math.max(m, Math.min(h - m, d.y)) } })

                lines.attr({
                    x1: function(d) { return d.source.x },
                    y1: function(d) { return d.source.y },
                    x2: function(d) { return d.target.x },
                    y2: function(d) { return d.target.y } })

                text.attr({ 
                    x: function(d) { return d.x },
                    y: function(d) { return d.y } }) })
            .on('end', function() { 
                force.nodes().forEach(function(n) { n.fixed = true })
            })
    }

    function stopSym () {
        force.stop()

        return graph
    }

    function canvas(_) { 
        if (!_) return chart.canvas

        chart.canvas = _

        return graph
    }

    function textFunctor (_) {
        if (!_) return textFunc

        textFunc = _

        return graph
    }

    return graph
}

return GraphChart

})(BiomartVisualization) 