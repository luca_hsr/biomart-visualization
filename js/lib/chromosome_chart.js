BiomartVisualization.ChromosomeChart = (function (bs, d3) {
"use strict"

var chroms = [
    // length is in millions
    { name: 1, length: 249 },
    { name: 2, length: 243 },
    { name: 3, length: 198 },
    { name: 4, length: 191 },
    { name: 5, length: 180 },
    { name: 6, length: 171 },
    { name: 7, length: 159 },
    { name: 8, length: 146 },
    { name: 9, length: 141 },
    { name: 10, length: 135 },
    { name: 11, length: 135 },
    { name: 12, length: 133 },
    { name: 13, length: 115 },
    { name: 14, length: 107 },
    { name: 15, length: 102 },
    { name: 16, length: 90 },
    { name: 17, length: 81 },
    { name: 18, length: 78 },
    { name: 19, length: 59 },
    { name: 20, length: 63 },
    { name: 21, length: 48 },
    { name: 22, length: 51 },
    { name: 'X', length: 155 },
    { name: 'Y', length: 59 },
    { name: 'Unknown', length: 300 }
]

function chromosomeChart(svg) {
    var legendItemGap = 10
    var color = d3.scale.category20()
    var perRow = 9
    var width = 18
    var chart = bs.Chart(svg)
    chart.settings({ text: true })
    var els

    var max = []
    for (var j = 0, l = Math.ceil(chroms.length/perRow); j < l; ++j)
        max[j] = d3.max(chroms.slice(j*perRow, (j+1)*perRow), function(c) { return c.length }) + 35

    var chromosomeChart = {
        draw: draw,
        addGene: addGene,
        removeGene: removeGene,
        canvas: canvas,
        max: max
    }

    d3.rebind(chromosomeChart, chart, 'on', 'translate', 'scale', 'resize', 'settings')

    function doDraw() {
        els = chart.els = chart.canvas.selectAll('g')
            .data(chroms)
        .enter()
            .append('g')
        
        els.append('rect')
            .attr({
                id: function(d) { return chromId(d.name) },
                width: width,
                height: function(d) { return d.length },
                x: function(d, i) { return d.x = (i % perRow) * 80 },
                y: function(d, i) { 
                    var c = Math.floor(i / perRow)
                    return d.y = c === 0 ? 0 : c === 1 ? max[0] : max[0]+max[1] },
                fill: function(d) { return color(d.name) } })

        els.append('text')
            .attr({
                x: function(d) { return d.x + width / 2 },
                y: function(d) { return d.y - 15 },
                'text-anchor': 'middle' })
            .text(function(d) { return d.name })

        els.append('g')
            .attr('id', function(d) { return legendId(d.name) })

        chart.translate(0, 60)
    }

    function canvas(_) { 
        if (!_) return chart.canvas

        chart.canvas = _

        return chromosomeChart
    }

    function draw() {
        doDraw()
        chart.addListeners()
    }

    function addGene(gene) {
        var geneName = gene["HGNC symbol"]
        var geneId = geneNameId(geneName)
        var c = gene["Chromosome Name"]
        var chrName
        var res = chroms.some(function(chromosome) { return (chrName = chromosome.name) == c })
        var g
        if (!res)
            chrName = 'Unknown'

        if (g = document.getElementById(geneId)) {
            ++g.__data__
            return
        }

        doDrawGene(chrName, geneName, Math.round(+gene["Gene Start (bp)"] / 1e6))
    }

    function doDrawGene(chrName, geneName, startPoint) {
        var chromLegend = document.getElementById(legendId(chrName))
        
        if (! chromLegend)
            throw new Error('No DOM element found with id: '+ name)

        var group = d3.select(chromLegend.parentNode)
        var geneOnChromId = geneMarkId(geneName)
        var chromosome = document.getElementById(chromId(chrName))
        var cBox = {
            x: +chromosome.getAttribute('x'),
            y: +chromosome.getAttribute('y'),
            width: +chromosome.getAttribute('width')
        }

        
        d3.select(chromLegend).append('text')
            .property('__data__', 1)
            .attr({
                id: geneNameId(geneName),
                "font-family": "sans-serif",   
                "font-size": 10,
                y: function(d) { 
                    var p = this.parentNode
                    // This node included.
                    var i = p.childNodes.length

                    return cBox.y + legendItemGap * i 
                },
                x: function(d) { return cBox.x + cBox.width + 5} })
            .on('mouseover', function() {
                var e = document.getElementById(geneOnChromId)

                e.setAttribute('stroke-width', 5)
                e.setAttribute('stroke', 'yellow') })
            .on('mouseout', function() {
                var e = document.getElementById(geneOnChromId)

                e.setAttribute('stroke-width', 1)
                e.setAttribute('stroke', 'black') }) 
            .text(geneName)

        group.append('path')
            .attr({
                id: geneOnChromId,
                d: hLine([cBox.x, cBox.y + startPoint], cBox.x + cBox.width),
                stroke: 'black',
                'stroke-width': 1
            })
    }

    function removeGene(gene) {
        var name = gene["HGNC symbol"]
        var geneId = geneNameId(name)
        var chromGeneId = geneMarkId(name)

        var e
        var ey
        var parent
        if (e = document.getElementById(geneId)) {
            if (--e.__data__ > 0) return

            ey = +e.getAttribute('y')
            parent = e.parentNode

            // d3.select(e).transition().remove()
            parent.removeChild(e)

            d3.selectAll(parent.childNodes)
                .filter(function() { 
                    return this.getAttribute('y') > ey })
                // .transition()
                // .duration(100)
                .attr('y', function(d, i) { 
                    return +this.getAttribute('y') - legendItemGap })
        }

        if (e = document.getElementById(chromGeneId))
            e.parentNode.removeChild(e)
    }

    var legendId = function(name) {
        return 'legend-'+name
    }

    var geneNameId = function(name) {
        return 'gene-name-'+name
    }

    var geneMarkId = function(name) {
        return 'position-'+name
    }

    var chromId = function(name) {
        return 'chromosome-'+ name
    }

    var hLine = function(start, end) {
        return 'M'+start[0]+','+start[1]+'L'+end+','+start[1]
    }

    return chromosomeChart
}

return chromosomeChart

})(BiomartVisualization, d3)