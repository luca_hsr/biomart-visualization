BiomartVisualization.DataHandler = (function (bs, d3) {
"use strict"

var slice = Array.prototype.slice

return {
    _queries: {
        MIM: '../mim_assoc.csv' // it could be an XML query or so
    },
    getData: function(query, callback) {
        if (typeof callback !== 'function') return
        if (! (query in this._queries)) {
            callback(null) // or [] ?
            return
        }
        var q = this._cache.getValue(query)
        if (!q) {
            var self = this
            d3.csv(this._queries[query], function(data) {
                q = data
                self._cache.setValue(query, q)
                callback(slice.call(q, 0))
            })
            return
        }
        callback(slice.call(q, 0))
    },
    _cache: { // this is just for example
        setValue: function(key, value) {
            this.c[key] = value
        },
        getValue: function(key) {
            return this.c[key]
        },
        c: {}
    }
}
})(BiomartVisualization, d3)
