BiomartVisualization.karyotype = (function (bs) {
"use strict"

function karyotype (svg, dataStruct, settings) {
    var chroms = bs.ChromosomeChart(svg)
    chroms.settings(settings.chartSettings)

    chroms.draw()
        // .resize(mainPanelWidthRate, function(newWidth, oldWidth) {
        //     newWidth / viewportW })
        // .scale(mainPanelWidth/viewportW, mainPanelWidth/viewportW)

    return chroms
}

return karyotype

})(BiomartVisualization)
