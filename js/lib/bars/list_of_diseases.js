BiomartVisualization.ListOfDiseases = (function (bs, d3) {
"use strict"

function listOfDiseases(svg, dataStruct, settings) {

    //
    // Devi contare lo stroke-width nella lunghezza della svg etc..
    //


    var widthRate = settings.widthRate || 0.3
    var heightRate = settings.heightRate || 0.8
    var hbars = bs.HBarChart(svg)
    hbars.settings(settings.chartSettings)
    var st = hbars.settings()
    var cScale
    var degree = dataStruct.degree
    var color = function(d) { return cScale(degree(d)) }
    var max
    var viewBox
    var divContainer = d3.select('#list-of-diseases')

    svg.attr({ height: st.thickness * dataStruct.data.length })

    viewBox = '0 0 '
        + svg.attr('width') + ' '
        + svg.attr('height')
    svg.attr('viewBox', viewBox)
    svg.attr('preserveAspectRatio', 'xMinYMin meet')

    max = hbars
        .data(dataStruct.data)
        .barLength(degree)
        .max()

    cScale = d3.scale.linear()
        .domain(settings.color.length > 2
                ? [0, max/2, max] : [0, max])
        .range(settings.color)

    hbars
        .color(color)
        .y(settings.y)
        .barLength(degree)
        .text(settings.text)
        .scale(svg.attr('width') / max, 1) // svg.attr('height') / max)
        // .resize(widthRate, function (newW) {
        //     // divContainer.style('height', (newH - 30) + 'px')
        //     return [newW / max, 1] })
        .draw()

    return hbars
}

return listOfDiseases

})(BiomartVisualization, d3)
