BiomartVisualization.HBarChart = (function (bs, d3) {
"use strict"

function hbarChart(svg) {
    var chart = bs.Chart(svg)
    chart.settings({
        thickness: 20,
        stroke: '#333333',
        'stroke-width': 1,
        text: false,
        opacity: 0.5
    })

    var bars 
    var max
    var colorScale
    var length
    var barY
    var _text
    var idFunc

    var hbarChart

    function maximum () {
        if (! max && bars) 
            max = d3.max(bars, length)

        return max
    }

    function draw() {
        doDraw()
        chart.addListeners()

        return hbarChart
    }

    // `value` is the property that define the bar length. It could be an integer
    function barLength (_) {
        if (! _) return length

        length = d3.functor(_)

        return hbarChart
    }

    function y (_) {
        if (!_) return barY

        barY = _

        return hbarChart
    }

    // or a function returning an integer and must be idempotent.
    function setData (data) {
        if (! data) return bars

        bars = data
        
        return hbarChart
    }

    function color (value) {
        if (! value) return colorScale

        colorScale = value

        return hbarChart
    }

    function text (_) {
        if (!_) return _text

        _text = _

        return hbarChart
    }

    function idFunctor (_) {
        if (!_) return idFunc

        idFunc = _

        return hbarChart
    }

    function doDraw() {
        var cs = chart.settings()
        var th = cs.thickness

        chart.canvas.attr({ 
            stroke: cs.stroke, 
            'stroke-width': cs['stroke-width'], 
        //     height: th,
        //     opacity: cs.opacity 
            x: 0
        })

        // The order is the same as is the bond g -> data. Draw just the 
        // missing ones.
        chart.els = chart.canvas.selectAll('g')
            .data(bars)

        chart.els.selectAll('rect')
            .attr({
                id: idFunc,
                width: length, 
                fill: colorScale })
            
        chart.els.selectAll('text')
            .text(_text) // TODO: if (text) then...

        var enter = chart.els.enter()
            .append('g')

        enter
            .append('rect')
            .attr({ 
                height: th,
                opacity: cs.opacity,
                id: idFunc,
                y: barY,
                width: length, fill: colorScale })

        if (cs.text) {
            enter
                .append('text')
                .attr({
                    x: 5,
                    y: function(d, i) { return (i + 2/3) * th },
                    "font-family": "sans-serif",   
                    "font-size": 10,
                    "text-anchor": "start",
                    fill: "black" })
                .text(_text)
        }

        chart.els.exit().remove()

        return hbarChart
    }

    function canvas(_) { 
        if (!_) return chart.canvas

        chart.canvas = _

        return hbarChart
    }

    // function resize (widthRate, scaleCallback) {
    //     resizer(function () {
    //         var canvasNode = chart.canvas.node()
    //         var svg = canvasNode.nearestViewportElement
    //         var newWidth = verge.viewportW() * widthRate
    //         svg.setAttribute('width', newWidth)

    //         // It should be a function
    //         if (scaleCallback) {
    //             var oldWidth = +svg.getAttribute('width')
    //             var scale = scaleCallback(newWidth, oldWidth)
    //             var transf = d3.transform(canvasNode.getAttribute('transform'))
    //             transf.scale = [scale]
    //             canvasNode.setAttribute('transform', transf.toString())                
    //         }
    //     })

    //     return hbarChart
    // }

    hbarChart = {
        canvas: canvas,
        draw: draw,
        data: setData,
        color: color,
        barLength: barLength,
        max: maximum,
        y: y,
        text: text,
        // resize: resize,
        id: idFunctor
    }

    return d3.rebind(hbarChart, chart, 'on', 'translate', 'scale', 'settings')
}

return hbarChart

})(BiomartVisualization, d3)
