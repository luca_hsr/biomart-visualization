
!function (d3, verge, _, bs) {

"use strict"

var metrics = bs.Metrics

var graphVisualization 

/*

var genes = d3.nest()
    .key(function (d) { return d["HGNC symbol"] })
    .rollup(function (leaves) { 
        return { 'n. of disease': leaves.length, diseases: leaves } 
    })

*/

 /*  List of Diseases  */

// in this case it isn't exactly correct calling the leaves, genes.
var diseases = d3.nest()
    .key(function(d) { return d["MIM Morbid Description"] })
    .rollup(function(leaves) { 
        return { 'n. of genes': leaves.length, genes: leaves } })


 /*  Karyotype  

    var svg3 = d3.select('#karyotype-tab')
    
    var kyrioSvg = svg3.append('svg').attr({ 
        width: mainPanelWidth, 
        height: mainPanelHeight 
    })

*/

var searcher = SearchBar('js/lib/searcher.js')

bs.DataHandler.getData('MIM', function(data) {
    if (!data) {
        throw new Error('no data!')
    }

    function newGraph () {
        if (!graphVisualization) {
            graphVisualization = 
                bs.Graphs.DiseaseGraph('#graph-tab', diseases.entries(data))
        } else {
            graphVisualization.remove()
        }

        graphVisualization
            .genes(diseaseBars.getGenesFromSelectedDisease())
            .draw()
    }

    function newBars (data) {
        return diseaseBars
            .remove()
            .data(data)
            .draw()
    }


    /*  bars chart  */
    var diseasesEntries = diseases.entries(data)
    var diseaseBars = bs.Bars.DiseaseBars(
        '#list-of-diseases', 
        diseasesEntries
    ).draw()

    document.getElementById('search')
        .addEventListener('keypress', function startDiseaseSearch (e) {
        if (e.keyCode !== 13) return

        var value = e.target.value
        if (value == '') {
            newBars(diseasesEntries)
                .getChart().on('click', newGraph)
            return             
        }

        searcher.search(e.target.value.toUpperCase(), diseasesEntries, function(data) { 
            newBars(data)
                .getChart().on('click', newGraph)
        })
    })

    /*  graph chart  */

    diseaseBars.getChart().on('click', newGraph)

    
    /*  chromosome  

    var chroms = karyotype(kyrioSvg)

    hbars.on('click', function(d) {
        var method = d.showingGenes ? 'addGene' : 'removeGene'
        d.values.genes.forEach(function(gene) {
            chroms[method](gene)   
        })
    })

    */

})

}(d3, verge, _, BiomartVisualization)
