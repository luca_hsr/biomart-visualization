
if (! BiomartVisualization.Graphs)
    BiomartVisualization.Graphs = Object.create(null)

BiomartVisualization.Graphs.DiseaseGraph = (function (bs, d3) {

"use strict"

var color = d3.scale.linear()
    .domain([0, 10, 20])
    .range(['#ffffff', '#777777', '#000000'])

var chartSettings =  { 
    nodeClassName: function(d) { return d.gene ? 'gene' : 'disease' }, 
    gravity: 0, 
    radius: 20,
    width: bs.Metrics.mainPanelWidth, 
    height: bs.Metrics.mainPanelHeight, 
    text: true, 
    linkDistance: function(link) {
        // return link.source.weight + link.target.weight > 8 ? 200 : 100
        if (link.source.weight > 4 ^ link.target.weight > 4)
            return 150
        if (link.source.weight > 4 && link.target.weight > 4)
            return 350
        return 100
    }
} 


// container: '#graph-tab'. d3 element or selector
function diseaseGraph (container, data) {

    /*  Graph generated clicking on a disease  */

    var svg = d3.select(container).append('svg')
        .attr({ width: bs.Metrics.mainPanelWidth, 
                height: bs.Metrics.mainPanelHeight, 
                id: 'graph' })

    // var selectedGeneClass = 'selected_gene'
    var genes
    var graph
    // TODO: show only the genes involved with the clicked disease and diseases
    // having the same genes.
    var nodes
    var edges

    // given the data for a gene retrieves and adds related diseases, making
    // the opportune links. 
    function nodesNedges(d, i) {
        var nodeIdx
        var alreadyEdge
        var inNodes
        var disease
        for (var gIdx = 0, l = genes.length; gIdx < l; ++gIdx) {
            disease = d.values.genes.some(function (gene) {
                return gene['HGNC symbol'] === genes[gIdx]['HGNC symbol']
            })

            if (disease) {
                inNodes = nodes.some(function (node, i) {
                    nodeIdx = i
                    return !node.gene && 
                        node.key === d.key
                })

                if (inNodes) {
                    alreadyEdge = edges.some(function (edge) {
                        return edge.source === gIdx && edge.target === nodeIdx
                    })                    
                } else {
                    nodeIdx = nodes.push(bs.Util.clone(d)) - 1
                }    

                if (! alreadyEdge)
                   edges.push({ source: gIdx, target: nodeIdx })

                alreadyEdge = false
            }
        }

        // var newDis, gi = nodes.length
        // d.gene = true

        // nodes.push(bs.Util.clone(d))

        // d.values.diseases.forEach(function(dis, di) {
        //     var t = _.findIndex(nodes, function(n) { 
        //                  return n.key === dis["MIM Morbid Description"] })

        //     if (t < 0) {
        //         newDis = _.find(dataStruct.data, function(d) { 
        //                     return d.key === dis["MIM Morbid Description"] })
        //         // the position of the new entry
        //         t = nodes.length
        //         nodes.push(newDis)
        //     }

        //     var e = _.findIndex(edges, function(e) {
        //                 return e.source === gi && e.target === t })

        //     if (e < 0)
        //         edges.push({ source: gi, target: t })
        // })
    }


    // var viewBox = '0 0 '
    //     + svg.attr('width') + ' '
    //     + svg.attr('height')
    // svg.attr('viewBox', viewBox)
    // svg.attr('preserveAspectRatio', 'xMinYMin meet')

     
    var g = {
        getGraph: function () {
            return graph
        },

        draw: function () {
            // genes.forEach(getDiseases)
            data.forEach(nodesNedges)
            graph = bs.GraphChart(svg)
                .settings(chartSettings)
                .nodes(nodes)
                .links(edges)
                .id(function (d) { return d.gene ? d['HGNC symbol'] : d.key })
                .color(function(d) {
                    return d.gene 
                        ? '#bcbd22' 
                        : color(d.values['n. of genes']) })
                .text(function (d) { return d.gene ? d['HGNC symbol'] : d.key })
                .draw()

            return g
        },

        remove: function () {
            graph.canvas().remove()
            graph = null

            return g
        },

        // argument must be a d3 selection
        genes: function (_) {
            if (!_) return genes

            genes = bs.Util.clone(_)
            nodes = genes.map(function (g) { g.gene = true; return g })
            edges = []

            return g
        }

        // BEGIN TEST API
        ,
        nodesNedges: nodesNedges,
        
        getNodes: function () {
            return nodes
        },
        
        getEdges: function () {
            return edges
        }

        // END TEST API
    }

    return g
}

return diseaseGraph

})(BiomartVisualization, d3)
