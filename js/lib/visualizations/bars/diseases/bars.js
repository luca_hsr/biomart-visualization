
if (! BiomartVisualization.Bars)
    BiomartVisualization.Bars = Object.create(null)

BiomartVisualization.Bars.DiseaseBars = (function (bs, d3) {

"use strict"

var chartSettings = { thickness: 25, text: true }
var width = (1 - bs.Metrics.mainPanelWidthRate - 0.15) * bs.Metrics.initialViewportW
var degree = function(d) { 
    return +d.values['n. of genes'] * 8 }

// '#list-of-diseases'
function diseaseBars (container, data) {

    var max
    var selection
    var hbars
    var collection = data

    function colorScale () {
        var colors = ['#0000ff', '#ff0000']
        return d3.scale.linear()
            .domain(colors.length > 2
                ? [0, max/2, max] 
                : [0, max])
            .range(colors)
    }

    // Given a disease returns all the unique genes involved with it.
    function getGenes (d) {
        selection = []

        var idQueue = []
        d.values.genes.forEach(function (d) { 
            if (idQueue.indexOf(d["HGNC symbol"]) < 0) {
                idQueue.push(d["HGNC symbol"])
                selection.push(d)
            }
        })

        return selection
    }

    function markDisease (d) {
        if (d.showingGenes = !d.showingGenes) 
            getGenes(d)
    }

    var svg = d3.select(container)
        .style('height', (bs.Metrics.initialViewportH - 30) + 'px')
        .append('svg')
        .attr({ width: width, id: 'bars' })

    function viewBox () {
        svg.attr('viewBox', '0 0 '+svg.attr('width')+' '+ svg.attr('height'))
        svg.attr('preserveAspectRatio', 'xMinYMin meet')
    }

    function sort (a, b) { 
        var aa = degree(a)
        var bb = degree(b)
        return bb < aa ? -1 : bb > aa ? 1 : 0 
    }

    var normalize = bs.Util.normalizeId

   /*  horizontal bar chart  */

    collection.sort(sort)

    var h = {
        draw: function () {
            hbars = bs.HBarChart(svg)
                .settings(chartSettings)
                .data(collection)
                .barLength(degree)

            max = hbars.max()
            svg.attr({ height: hbars.settings().thickness * data.length })

            var cScale = colorScale()

            hbars
                .color(function (d) { return cScale(degree(d)) })
                .y(function(d, i) { return i * 25 })
                .barLength(degree)
                .text(function(d) { return d.key })
                .scale(svg.attr('width') / max, 1)
                .id(function (d) { return normalize(d.key) })
                // .resize(widthRate, function (newW) {
                //     // divContainer.style('height', (newH - 30) + 'px')
                //     return [newW / max, 1] })
                .on('click', markDisease)
                .draw()

            return h
        },

        data: function (_) {
            if (!_) return collection

            collection = _.sort(sort)

            return h    
        },

        getGenesFromSelectedDisease: function () {
            return selection 
        },

        getChart: function () {
            return hbars
        },

        getBars: function () {
            return hbars.canvas().selectAll('rect')[0]
        },

        remove: function () {
            hbars.canvas().remove()
            hbars = null

            return h
        }

        // BEGIN TEST API
        ,
        getGenes: getGenes,

        markDisease: markDisease

        // END TEST API
    }

    return h
}

return diseaseBars

})(BiomartVisualization, d3)