BiomartVisualization.Chart = (function (bs, d3) {
"use strict"

var slice = Array.prototype.slice

function Chart(svg) {
    if (! (svg instanceof d3.selection)) {
        svg = d3.select(svg)
    }

    var canvas
    var events = []
    var els = null
    var opt = {}
    var merged = false
    var evtCounter = 0

    var tag = svg.property('tagName')

    if (tag === 'svg') {
        canvas = svg.append('g')
    } else if (tag === 'g') {
        canvas = svg
    } else {
        throw new Error('no `<svg>`/`<g>` element found')
    }


    var chart = {
        settings: settings,
        addListeners: addListeners,
        on: on,
        translate: translate,
        scale: scale,
        resize: resize
    }

    Object.defineProperty(chart, 'els', {
        get: function() { return els },
        set: function(value) { return els = value },
        configurable: true,
        enumerable: true
    })

    Object.defineProperty(chart, 'canvas', {
        get: function() { return canvas },
        set: function(value) { return canvas = value },
        configurable: true,
        enumerable: true
    })

    function settings (sett) {
        if (sett) {
            _.merge(opt, sett)
            return chart
        }

        return opt
    }

    function addListeners() {
        for (var type in events) {
            var ls = events[type]
            ls.forEach(function(listener) { els.on(type+'.'+evtCounter++, listener) })
            delete events[type]
        }

        return chart
    }

    function on(type, listener) {
        var exLis, lis = listener
        
        if (els) {
            if (exLis = els.on(type))
                lis = function() { 
                    exLis.apply(this, slice.call(arguments, 0))
                    listener.apply(this, slice.call(arguments, 0)) }
            els.on(type, lis)
        }
        else 
            events[type] 
                ? events[type].push(listener)
                : events[type] = [listener]
        return chart
    }

    function translate(x, y) {
        canvas.attr('transform', transform() + bs.Util.translate(x, y))
        return chart
    }
    
    function scale(x, y) {
        canvas.attr('transform', transform() + bs.Util.scale(x, y))        
        return chart
    }
    
    function transform() {
        var tr = canvas.attr('transform')
        return tr ? tr+' ' : ' '
    }

    // `rate` is a number in the interval [0, 1] and the scale for svg's width
    // `scaleFactor` is the scale for the group.
    function resize (widthRate, heightRate, scaleCallback) {
        bs.Resizer(function () {
            var canvasNode = canvas.node()
            var svg = canvasNode.nearestViewportElement
            var newWidth = verge.viewportW() * widthRate
            var newHeight = verge.viewportH() * heightRate
            svg.setAttribute('width', newWidth)
            svg.setAttribute('height', newHeight)

            // It should be a function
            if (scaleCallback) {
                var oldWidth = +svg.getAttribute('width')
                var oldHeight = +svg.getAttribute('height')
                var scale = scaleCallback(newWidth, newHeight, oldWidth, oldHeight)
                var transf = d3.transform(canvasNode.getAttribute('transform'))
                transf.scale = scale
                canvasNode.setAttribute('transform', transf.toString())                
            }
        })
        
        return chart
    }

    return chart
}

return Chart

})(BiomartVisualization, d3)
